package ke.co.mtek;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.os.Bundle;
import android.view.MenuItem;



import com.google.android.material.bottomnavigation.BottomNavigationView;


public class Main3Activity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        BottomNavigationView navview = findViewById(R.id.bottom_navigation);
        navview.setOnNavigationItemSelectedListener(navListener);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame, new home_fragment()).commit();
    }



    private BottomNavigationView.OnNavigationItemSelectedListener  navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment myFrag = null;

            switch (menuItem.getItemId()){
                case R.id.home:
                    myFrag = new home_fragment();
                    break;
                case R.id.notification:
                    myFrag = new notification();
                    break;
                case R.id.profile:
                    myFrag = new profile();
                    break;
            }

            assert myFrag != null;
            getSupportFragmentManager().beginTransaction().replace(R.id.frame, myFrag).commit();

            return true;

        }
    };

}