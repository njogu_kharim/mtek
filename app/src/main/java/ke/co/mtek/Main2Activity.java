package ke.co.mtek;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {
 private Button button;
 private TextView userName;
 private TextView pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sign_in();
            }
        });
    }

    public void Sign_in(){
        Intent intent=new Intent(this, Main3Activity.class);
        startActivity(intent);

    }
    public void onClick(View x) {
        String userNameTxt = this.userName.getText().toString();
        String passTxt = this.pass.getText().toString();
        Log.e("Main2Activity", "username == " +
                userNameTxt +
                "password == " +
                passTxt);
    }
 }
