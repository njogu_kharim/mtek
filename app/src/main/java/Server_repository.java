import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Server_repository {
    private IServerWUService ws;

    public Server_repository(){
        Retrofit retro= new Retrofit.Builder()
                            .baseUrl("https://hillcroftinsurance.com:8445")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
        this.ws= retro.create(IServerWUService.class);

    }
    public void getStatus(){
        this.ws.getStatus().enqueue(new Callback<server>() {
            @Override
            public void onResponse(Call<server> call, Response<server> response) {
            server s= response.body();
                Log.e("SRES",s.getStatus());
            }

            @Override
            public void onFailure(Call<server> call, Throwable t) {

            }
        });
    }
}
